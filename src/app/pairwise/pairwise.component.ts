import { Component, OnInit } from '@angular/core';
import { pairwise } from 'rxjs/operators';
import { HorizontalService } from '../horizontal.service';

@Component({
  selector: 'app-pairwise',
  templateUrl: './pairwise.component.html',
  styleUrls: ['./pairwise.component.scss']
})
export class PairwiseComponent implements OnInit {

  values: number[] = [0, 0];

  constructor(
    private service: HorizontalService
  ) { }

  ngOnInit(): void {

    this.service.getRamdomValueWInterval(0, 10, 3000, 1000)
      .pipe(
        pairwise()
      )
      .subscribe({
        next: ([prev, curr]) => {

          this.values = [prev, curr];
        }
      });
  }

}
