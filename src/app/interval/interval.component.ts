import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';

@Component({
  selector: 'app-interval',
  templateUrl: './interval.component.html',
  styleUrls: ['./interval.component.scss']
})
export class IntervalComponent implements OnInit {

  value = 0;

  constructor() { }

  ngOnInit(): void {

    interval(1000)
      .subscribe({
        next: n => {
          this.value = n;
        }
      });


  }

}
