import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-item-behavior',
  templateUrl: './item-behavior.component.html',
  styleUrls: ['./item-behavior.component.scss']
})
export class ItemBehaviorComponent implements OnInit {

  @Input() valueSubject$: BehaviorSubject<number>;

  value: number;

  constructor() { }

  ngOnInit(): void {

    this.valueSubject$
      .subscribe({
        next: value => {

          this.value = value;

        }
      });

  }

}
