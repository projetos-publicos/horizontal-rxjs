import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemBehaviorComponent } from './item-behavior.component';

describe('ItemBehaviorComponent', () => {
  let component: ItemBehaviorComponent;
  let fixture: ComponentFixture<ItemBehaviorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemBehaviorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemBehaviorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
