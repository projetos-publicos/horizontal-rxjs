import { Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-behavior-subject',
  templateUrl: './behavior-subject.component.html',
  styleUrls: ['./behavior-subject.component.scss']
})
export class BehaviorSubjectComponent {

  valueSubject$: BehaviorSubject<number>;

  constructor() {
    this.valueSubject$ = new BehaviorSubject(5);
  }

  increment(): void {

    let value = this.valueSubject$.value;

    this.valueSubject$.next(++value);
  }

}
