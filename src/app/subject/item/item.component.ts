import { Component, Input, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

  @Input() valueSubject$: Subject<number>;

  value: number;

  constructor() { }

  ngOnInit(): void {

    this.valueSubject$
      .subscribe({
        next: value => {
          this.value = value;
        }
      });
  }

}
