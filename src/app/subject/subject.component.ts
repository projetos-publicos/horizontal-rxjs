import { Component } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.scss']
})
export class SubjectComponent {

  valueSubject$: Subject<number>;

  value = 0;

  constructor() {
    this.valueSubject$ = new Subject();
  }

  increment(): void {
    this.valueSubject$.next(++this.value);
  }

}
