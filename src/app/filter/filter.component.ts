import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { filter, map } from 'rxjs/operators';
import { HorizontalService } from '../horizontal.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  onlyEven: number[] = [];
  allNumbers: number[] = [];

  constructor(
    private service: HorizontalService
  ) { }

  ngOnInit(): void {
    this.loadNumbers()
  }

  loadNumbers(): void {

    this.service.getRamdomValueWInterval(0, 20, 1000, 20)
      .pipe(
        filter(n => {
          this.allNumbers.push(n);

          return (n % 2 === 0);
        })
      )
      .subscribe({
        next: n => {
          this.onlyEven.push(n);
        }
      });

  }

}
