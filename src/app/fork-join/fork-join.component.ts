import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { HorizontalService } from '../horizontal.service';

@Component({
  selector: 'app-fork-join',
  templateUrl: './fork-join.component.html',
  styleUrls: ['./fork-join.component.scss']
})
export class ForkJoinComponent implements OnInit {

  americaPopulation = 0;
  peoplePerArea = 0;

  constructor(
    private service: HorizontalService
  ) { }

  ngOnInit(): void {
    this.loadAmericaPopulationNumber();
  }

  loadAmericaPopulationNumber(): void {
    const north$ = this.service.getNorthAmericaPopulation();
    const central$ = this.service.getCentralAmericaPopulation();
    const south$ = this.service.getSouthAmericaPopulation();

    forkJoin([north$, central$, south$])
      .subscribe({
        next: ([north, central, south]) => {
          this.americaPopulation = north + central + south;
          this.peoplePerArea = Math.round(this.americaPopulation / 42550000);
        }
      });
  }

}
