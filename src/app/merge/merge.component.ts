import { Component, OnInit } from '@angular/core';
import { merge } from 'rxjs';
import { HorizontalService } from '../horizontal.service';

@Component({
  selector: 'app-merge',
  templateUrl: './merge.component.html',
  styleUrls: ['./merge.component.scss']
})
export class MergeComponent implements OnInit {

  ramdomNumbers: number[] = [];
  currentNumber: number;

  constructor(
    private service: HorizontalService
  ) { }

  ngOnInit(): void {
    this.loadNumbers();
  }

  loadNumbers(): void {
    const first$ = this.service.getRamdomValueWInterval(0, 10, 2000, 3);
    const second$ = this.service.getRamdomValueWInterval(11, 20, 3500, 3);
    const third$ = this.service.getRamdomValueWInterval(21, 30, 4500, 3);
    const fourth$ = this.service.getRamdomValueWInterval(31, 50, 5000, 3);

    merge(first$, second$, third$, fourth$)
      .subscribe({
        next: value => {
          this.currentNumber = value;
          this.ramdomNumbers.push(value);
        }
      });
  }

  refresh(): void {
    this.ramdomNumbers = [];
    this.currentNumber = null;
    this.loadNumbers();
  }

}
