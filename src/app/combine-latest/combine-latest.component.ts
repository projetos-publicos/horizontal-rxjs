import { Component, OnInit } from '@angular/core';
import { combineLatest, Subscription } from 'rxjs';
import { Barchart, HorizontalService } from '../horizontal.service';

@Component({
  selector: 'app-combine-latest',
  templateUrl: './combine-latest.component.html',
  styleUrls: ['./combine-latest.component.scss']
})
export class CombineLatestComponent implements OnInit {

  chartData: Barchart[] = [];
  history: Barchart[][] = [];

  view: any[] = [700, 400];

  // options
  showXAxis = true;
  showYAxis = true;
  showXAxisLabel = true;
  showYAxisLabel = true;
  xAxisLabel = 'Continente';
  yAxisLabel = 'Quantidade de acessos';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  subscrition: Subscription;
  paused = true;

  constructor(
    private service: HorizontalService
  ) {
  }

  ngOnInit(): void {
    this.getCartData();
  }

  getCartData(): void {
    this.paused = false;
    const america$ = this.service.getAmericaData();
    const europe$ = this.service.getEuropeData();
    const africa$ = this.service.getAfricaData();

    this.subscrition = combineLatest([america$, europe$, africa$])
      .subscribe({
        next: ([america, europe, africa]) => {

          this.chartData = [america, europe, africa];
          this.history.push(this.chartData);

        }
      });
  }

  freeze(): void {
    this.subscrition.unsubscribe();
    this.paused = true;
  }
}
