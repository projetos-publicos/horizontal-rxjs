import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { interval, Observable, of } from 'rxjs';
import { map, take } from 'rxjs/operators';

export interface Barchart {
    name: string;
    value: number;
}

export interface Address {
    cep: string,
    logradouro: string;
    complemento: string;
    bairro: string;
    localidade: string;
    uf: string;
}

@Injectable({
    providedIn: 'root'
})
export class HorizontalService {

    constructor(
        private httpClient: HttpClient
    ) { }

    getAmericaData(): Observable<Barchart> {
        return interval(2000)
            .pipe(
                map(() => {
                    return ({
                        name: 'America',
                        value: this.generateRamdomValue(0, 200)
                    });
                })
            );
    }

    getEuropeData(): Observable<Barchart> {
        return interval(4500)
            .pipe(
                map(() => {
                    return ({
                        name: 'Europa',
                        value: this.generateRamdomValue(0, 200)
                    });
                })
            );
    }

    getAfricaData(): Observable<Barchart> {
        return interval(5500)
            .pipe(
                map(() => {
                    return ({
                        name: 'Africa',
                        value: this.generateRamdomValue(2, 200)
                    });
                })
            );
    }

    getNorthAmericaPopulation(): Observable<number> {
        return of(579000000);
    }
    getCentralAmericaPopulation(): Observable<number> {
        return of(47448333);
    }
    getSouthAmericaPopulation(): Observable<number> {
        return of(422500000);
    }

    getRamdomValue(min: number, max: number): Observable<number> {
        return of(this.generateRamdomValue(min, max));
    }

    getRamdomValueWInterval(min: number, max: number, time: number, maxTimes: number): Observable<number> {
        return interval(time)
            .pipe(
                take(maxTimes),
                map(() => {
                    return this.generateRamdomValue(min, max);
                }),
            );
    }

    getDataFromFakeApi(): Observable<{ name: string; age: number }> {
        return this.httpClient.get<{ name: string; age: number }>('fake/api');
    }

    getAddressByCep(cep: string): Observable<Address> {
        return this.httpClient.get<Address>(`https://viacep.com.br/ws/${cep}/json/`);
    }

    private generateRamdomValue(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min)) + min;
    }
}
