import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HorizontalService } from '../horizontal.service';

@Component({
  selector: 'app-catch-error',
  templateUrl: './catch-error.component.html',
  styleUrls: ['./catch-error.component.scss']
})
export class CatchErrorComponent implements OnInit {

  data: { name: string; age: number };

  constructor(
    private service: HorizontalService
  ) { }

  ngOnInit(): void {
    this.loadDataFromApi();
  }

  loadDataFromApi(): void {

    this.service.getDataFromFakeApi()
      .pipe(
        catchError(err => {
          console.log(err);

          return of({ name: 'Não foi possível obter', age: 0 });
        })
      )
      .subscribe({
        next: value => {
          this.data = value;
        },
        error: err => {
          console.log('Algum erro aconteceu!');
          console.log(err);
        }
      });

  }

}
