import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { AppComponent } from './app.component';
import { BehaviorSubjectComponent } from './behavior-subject/behavior-subject.component';
import { ItemBehaviorComponent } from './behavior-subject/item/item-behavior.component';
import { CatchErrorComponent } from './catch-error/catch-error.component';
import { CombineLatestComponent } from './combine-latest/combine-latest.component';
import { DebounceTimeComponent } from './debounce-time/debounce-time.component';
import { FilterComponent } from './filter/filter.component';
import { ForkJoinComponent } from './fork-join/fork-join.component';
import { FromEventComponent } from './from-event/from-event.component';
import { HomeComponent } from './home/home.component';
import { IntervalComponent } from './interval/interval.component';
import { MapComponent } from './map/map.component';
import { MergeComponent } from './merge/merge.component';
import { PairwiseComponent } from './pairwise/pairwise.component';
import { ItemComponent } from './subject/item/item.component';
import { SubjectComponent } from './subject/subject.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'home',
    redirectTo: '',
    pathMatch: 'full'
  },
  {
    path: 'combine-latest',
    component: CombineLatestComponent
  },
  {
    path: 'fork-join',
    component: ForkJoinComponent
  },
  {
    path: 'merge',
    component: MergeComponent
  },
  {
    path: 'from-event',
    component: FromEventComponent
  },
  {
    path: 'interval',
    component: IntervalComponent
  },
  {
    path: 'pairwise',
    component: PairwiseComponent
  },
  {
    path: 'catch-error',
    component: CatchErrorComponent
  },
  {
    path: 'map',
    component: MapComponent
  },
  {
    path: 'filter',
    component: FilterComponent
  },
  {
    path: 'debounce-time',
    component: DebounceTimeComponent
  },
  {
    path: 'subject',
    component: SubjectComponent
  },
  {
    path: 'behavior-subject',
    component: BehaviorSubjectComponent
  },
];

@NgModule({
  declarations: [
    AppComponent,
    CombineLatestComponent,
    HomeComponent,
    ForkJoinComponent,
    MergeComponent,
    FromEventComponent,
    IntervalComponent,
    PairwiseComponent,
    CatchErrorComponent,
    MapComponent,
    FilterComponent,
    DebounceTimeComponent,
    SubjectComponent,
    ItemComponent,
    BehaviorSubjectComponent,
    ItemBehaviorComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    RouterModule.forRoot(routes),
    NgxChartsModule,
    MatChipsModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
