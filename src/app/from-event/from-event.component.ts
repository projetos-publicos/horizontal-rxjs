import { Component, OnInit } from '@angular/core';
import { fromEvent } from 'rxjs';

@Component({
  selector: 'app-from-event',
  templateUrl: './from-event.component.html',
  styleUrls: ['./from-event.component.scss']
})
export class FromEventComponent implements OnInit {

  coordinates: number[] = [];

  constructor() { }

  ngOnInit(): void {
    this.createClickEvent();
  }

  createClickEvent(): void {

    fromEvent(document, 'click')
      .subscribe({
        next: (value: MouseEvent) => {
          this.coordinates = [value.clientX, value.clientY];
        }
      });
  }

}
