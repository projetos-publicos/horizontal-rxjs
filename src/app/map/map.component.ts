import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { map } from 'rxjs/operators';
import { HorizontalService } from '../horizontal.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  street: string;
  inputControl = new FormControl('', [Validators.required, Validators.maxLength(8)]);

  constructor(
    private service: HorizontalService
  ) { }

  ngOnInit(): void {
  }

  loadCep(): void {

    const cep = this.inputControl.value;

    this.service.getAddressByCep(`${cep}`)
      .pipe(
        map(address => {

          console.log(address);

          return address.logradouro;
        })
      )
      .subscribe({
        next: street => {
          this.street = street;
        },
        error: err => {
          console.log(err);
        }
      });

  }

}
