import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-debounce-time',
  templateUrl: './debounce-time.component.html',
  styleUrls: ['./debounce-time.component.scss']
})
export class DebounceTimeComponent implements OnInit {

  inputControl = new FormControl('');

  value: string;

  constructor( ) { }

  ngOnInit(): void {
    this.inputChangesHandler()
  }

  inputChangesHandler(): void {

    this.inputControl.valueChanges
      .pipe(
        debounceTime(500)
      )
      .subscribe({
        next: value => {
          this.value = value;
        }
      });

  }

}
